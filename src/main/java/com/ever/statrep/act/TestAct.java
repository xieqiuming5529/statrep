package com.ever.statrep.act;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ever.statrep.dao.CustomerRepository;
import com.ever.statrep.mongodb.model.Customer;
import com.mongodb.BasicDBObject;

@Controller
@RequestMapping("/test")
public class TestAct {
	@Autowired
    private CustomerRepository repository;
	@Autowired  
	MongoTemplate mongoTemplate;
	
	@RequestMapping("/a")
	public @ResponseBody String greeting(Model model) {
		Criteria c = new Criteria();  
	    c.and("sex").is("F");  
	      
	    Aggregation aggr = Aggregation.newAggregation(  
	            Aggregation.match(c),
	            Aggregation.group("lastName").count().as("count")
	    );
	    AggregationResults<BasicDBObject> aggrResult = mongoTemplate.aggregate(aggr, "person", BasicDBObject.class);  
	    if(!aggrResult.getMappedResults().isEmpty()){  
	        for(BasicDBObject obj : aggrResult.getMappedResults()){  
	            	//logger.info("count by first name: {}", objectMapper.writeValueAsString(obj));  
	        }  
	    } 
		Customer customer = new Customer("12323","eh54y54");
		//customer
		repository.save(customer);
		Customer customer2 = repository.findByFirstName("123");
	    return "Hello World!sdfd";
	}
}

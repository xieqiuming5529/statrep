/**
 * 
 */
package com.ever.statrep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/** 
 * @author dsp
 * @Date 2017-9-12 下午3:02:12
 * @Version 0.1.0
 */
@SpringBootApplication
public class StatrepApplication extends SpringBootServletInitializer {

	/**
	 * @param args
	 * @author dsp
	 * @Date 2017-9-12 下午3:02:12
	 */
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(StatrepApplication.class);
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(StatrepApplication.class, args);
	}

}
